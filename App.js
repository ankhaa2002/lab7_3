import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PresentationalComponent from './src/screens/PresentationalComponent';
export default class App extends React.Component {
    state = {
        myState: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, used do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nullapariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officiadeserunt mollit anim id est laborum.'
    }
    updateState = () => {
        this.setState({ myState: 'The state is updated!!!' })
    }
    render() {
        return (
            <View>
                <PresentationalComponent myState={this.state.myState} updateState={this.updateState} />
            </View>
        );
    }
}
export default App;